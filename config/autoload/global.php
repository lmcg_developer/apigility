<?php
return array(
    'db' => array(
        'adapters' => array(
            'oauthMySql' => array(),
        ),
    ),
    'router' => array(
        'routes' => array(
            'oauth' => array(
                'options' => array(
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/oauth))',
                ),
                'type' => 'regex',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'map' => array(
                'ping\\V1' => 'oauth2_pdo',
                'ping\\V2' => 'oauth2_pdo',
                'Konoha\\V1' => 'oauth2_pdo',
                'Usuarios\\V1' => 'oauth2_pdo',
                'docente\\V1' => 'oauth2_pdo',
            ),
        ),
    ),
);
