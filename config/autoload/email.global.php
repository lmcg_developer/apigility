<?php

/*
 'email_accounts' => array(
        'cambiar_clave' => array(
            'config_server' => array(
                'name' => 'localhost.localdomain',
                'host' => 'mail.gowebperu.com',
                'port' => 25,
                'connection_class' => 'plain',
                'connection_config' => array(
                    'username' => 'carlos.sing@gowebperu.com',
                    'password' => '$ing&$had0w',)
            ),
            'config_message' => array(
                'name' => 'Evaluación docente',
                'from' => 'mensajeriadied5@perueduca.pe',
                'subject' => 'Cambio de Contraseña - Concursos Públicos de ingreso a la Carrera Pública Magisterial y de Contratación Docente en Instituciones Educativas Públicas De Educación Básica',
            ),
        )
    ),
  */

return array(
    'email_server' => array(
        'server_died' => array(
            'name' => 'localhost',
            'host' => 'mail.gowebperu.com',
            'port' => '25',
            'connection_class' => 'plain',
            'connection_config' => array(
                'username' => 'carlos.sing@gowebperu.com',
                'password' => '$ing&$had0w'
            ),
        )
    )
);