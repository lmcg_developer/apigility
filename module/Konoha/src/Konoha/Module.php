<?php

namespace Konoha;

use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface {

    public function getConfig() {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'ZF\Apigility\Autoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }

    // oauthMySql

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Konoha\V1\Rest\Password\PasswordMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Password\PasswordMapper($adapter);
                },
                'Konoha\V1\Rest\Models\UsuarioMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Models\UsuarioMapper($adapter);
                },
                'Konoha\V1\Rest\Comite\ComiteMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Comite\ComiteMapper($adapter);
                },
                'Konoha\V1\Rest\Docente\DocenteMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Docente\DocenteMapper($adapter);
                },
                'Konoha\V1\Rest\Models\OauthUserMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Models\OauthUserMapper($adapter);
                },
                'Konoha\V1\Rest\Models\BodyCorreoMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Models\BodyCorreoMapper($adapter);
                },
                'Konoha\V1\Rest\Models\UsersAclMapper' => function ($sm) {
                    $adapter = $sm->get('db');
                    return new \Konoha\V1\Rest\Models\UsersAclMapper($adapter);
                },
            ),
        );
    }

}
