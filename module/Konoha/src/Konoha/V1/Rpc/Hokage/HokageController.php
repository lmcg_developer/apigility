<?php

namespace Konoha\V1\Rpc\Hokage;

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ContentNegotiation\ViewModel;

class HokageController extends AbstractActionController {

    public function hokageAction() {

        return new ViewModel(array(
            'datos'=> array('minato', 'hashirama', 'naruto')
        ));
    }

}
