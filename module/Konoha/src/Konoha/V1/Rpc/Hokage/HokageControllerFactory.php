<?php
namespace Konoha\V1\Rpc\Hokage;

class HokageControllerFactory
{
    public function __invoke($controllers)
    {
        return new HokageController();
    }
}
