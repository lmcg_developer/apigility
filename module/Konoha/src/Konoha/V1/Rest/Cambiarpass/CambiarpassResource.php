<?php

namespace Konoha\V1\Rest\Cambiarpass;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class CambiarpassResource extends AbstractResourceListener {

    protected $mapper;
    protected $config;

    public function __construct($mapper, $config) {
        $this->mapper = $mapper;
        $this->config = $config;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data) {

//        return $data;
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id) {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data) {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id) {
        return array('id' => $id);
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array()) {

        return array('data' => $params);
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data) {
        $data = array(
            'id' => $id,
            'data' => $data
        );
        return $data;
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data) {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data) {
//        $data = array(
//            'id' => $id,
//            'data' => $data
//        );
//        return $data;

//        return $this->mapper->getBodyCorreo(20);
        return $this->mapper->cambiar_correo($id, $data->email);
//        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }

}
