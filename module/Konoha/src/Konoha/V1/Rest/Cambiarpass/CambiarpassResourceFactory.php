<?php

namespace Konoha\V1\Rest\Cambiarpass;

class CambiarpassResourceFactory {

    public function __invoke($services) {
        $mapper = $services->get('Konoha\V1\Rest\Models\OauthUserMapper');
        $config = $services->get('config');
        return new CambiarpassResource($mapper, $config);
    }

}
