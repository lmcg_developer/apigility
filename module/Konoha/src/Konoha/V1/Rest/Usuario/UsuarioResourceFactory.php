<?php
namespace Konoha\V1\Rest\Usuario;

class UsuarioResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Konoha\V1\Rest\Models\UsuarioMapper');
        return new UsuarioResource($mapper);
//        return new UsuarioResource();
    }
}
