<?php

namespace Konoha\V1\Rest\Password;

class PasswordEntity {

    
    public $id;
    public $username;

    public function getArrayCopy() {
        return array(
            'username' => $this->username,
            'id' => $this->username,
        );
    }

    public function exchangeArray(array $array) {
        $this->id = $array['username'];
        $this->username = $array['username'];
    }

}
