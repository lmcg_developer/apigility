<?php
namespace Konoha\V1\Rest\Password;

class PasswordResourceFactory
{
    public function __invoke($services)
    {
//        $mapper = $services->get('Konoha\V1\Rest\Models\UserMapper');
        $mapper = $services->get('Konoha\V1\Rest\Password\PasswordMapper');
        $config =  $services->get('config');
        return new PasswordResource($mapper, $config);
    }
    
//    public function __invoke($services)
//    {
//        return new PasswordResource();
//    }
}
