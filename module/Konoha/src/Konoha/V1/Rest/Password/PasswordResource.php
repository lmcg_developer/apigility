<?php

namespace Konoha\V1\Rest\Password;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Crypt\Password\Bcrypt;

class PasswordResource extends AbstractResourceListener {

    protected $mapper;
    protected $config;

    public function __construct($mapper, $config) {
        $this->mapper = $mapper;
        $this->config = $config;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data) {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id) {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data) {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id) {

        $url = $this->getEvent()->getQueryParam('url', '');

//        var_dump($url);
        $response = new \stdClass();
        $response->codigo = 0;
        $password = $this->mapper->generateToken($id);

        if ($password === FALSE) {
            $response->codigo = -101;
            $response->mensaje = 'No se encontró el usuario';
            return $response;
        }
        if ($password['email'] === '') {
            $response->codigo = -100;
            $response->mensaje = 'No tiene correo registrado';
            return $response;
        }
        if (strlen($url) === 0) {
            $response->codigo = -102;
            $response->mensaje = 'Ingrese una url para el token';
            return $response;
        }


        $config_email_server = $this->config['email_accounts']['cambiar_clave']['config_server'];
        $config_email_message = $this->config['email_accounts']['cambiar_clave']['config_message'];
// EMAIL
        $message = new Message();

        $htmlMarkup = 'Hola <b>' . $password['first_name'] . ': </b> '
                . '<p>Por favor visita el siguiente enlace para que puedas cambiar tu contraseña: </p>'
                . '<a href="' . $url . $password['token_tmp'] . '&username=' . $password['username'] . '" >' . $url . $password['token_tmp'] . '&username=' . $password['username'] . '</a>';

        $html = new MimePart($htmlMarkup);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($html));

        $message->addTo($password['email'])
                ->addFrom($config_email_message['from'])
                ->setSubject($config_email_message['subject'])
                ->setBody($body);

        $transport = new SmtpTransport();

        $options = new SmtpOptions($config_email_server);
//        $options = new SmtpOptions(array(
//            'name' => 'localhost.localdomain',
//            'host' => 'mail.singlabz.net',
//            'port' => 25,
//            'connection_class' => 'plain',
//            'connection_config' => array(
//                'username' => 'csing@singlabz.net',
//                'password' => '$ing&$had0w',
//            ),
//        ));

        try {
            $transport->setOptions($options);
            $transport->send($message);

            $password['codigo'] = 100;
        } catch (\Zend\Mail\Transport\Exception $e) {
            echo $e->getMessage();
        }


        return $password;

        //////
        //$htmlMarkup = 'PRUEBA';
//        $html = new MimePart($htmlMarkup);
//        $html->type = "text/html";
//
//        $body = new MimeMessage();
//        $body->setParts(array($html));
//
//        $message->addTo('carlossing@gmail.com')
//                ->addFrom('csing@singlabz.net')
//                ->setSubject('Se ha registrado su reclamo')
//                ->setBody($body);
//
//        $transport = new SmtpTransport();
//        $options = new SmtpOptions(array(
//            'name' => 'singlabz.net',
//            'host' => 'mail.singlabz.net',
//            'port' => 25,
//        ));
        //////
//        $demo = new \stdClass();
//        $demo->dni = $id;
//        $demo1 = array($demo);
//        return $demo1;
        /* return new ApiProblem(405, 'The GET method has not been defined for individual resources'); */
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array()) {


//        $password = $this->mapper->generateToken($id);
//        return $params;
//        $demo = new \stdClass();
//        $demo->email = 'carlossing@gmail.com';
//        $demo1 = array($demo);
//        return $demo1;
        return new ApiProblem(405, 'The GET method has not been defined for collections 132');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data) {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data) {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data) {

//        $bcrypt = new Bcrypt();
//        $valida = $this->mapper->validar_clave_anterior($data->username);
//        if ($valida !== false) {
//            $desencripta = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, '35e907c7bd0e7619ced8e8a1651ccecb', base64_decode($valida['password']), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
//            if ($desencripta === $data->password_anterior) {
//                $pass_bcrypt = $bcrypt->create(strtoupper($data->password_nuevo));
//                $password = $this->mapper->change_password_x_username($data->username, $data->password_nuevo, $pass_bcrypt);
//            } else {
//                return false;
//            }
//        }
//        return $password;
        
        $bcrypt = new Bcrypt();
        $pass_bcrypt = $bcrypt->create(strtoupper($data->password_nuevo));
        $password = $this->mapper->change_password_x_username($data->username, $data->password_nuevo, $pass_bcrypt);

        return $password;
    }

}
