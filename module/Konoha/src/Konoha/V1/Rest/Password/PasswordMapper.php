<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Password;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;

/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class PasswordMapper {

    protected $adapter;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function fetchOne($dni) {
        $sql = 'SELECT username, email FROM oauth_users WHERE username = ?';
//        $sql = 'SELECT * FROM oauth_users WHERE username = :dni';
        $resultset = $this->adapter->query($sql, array($dni));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }

//        $entity = new PasswordEntity();
//        $entity->exchangeArray($data[0]);
        return $data;
    }

    public function generateToken($dni) {

        $token = md5(time() . 'MINEDU');
        $sql = 'update oauth_users '
                . 'set token_tmp = ?'
                . 'where username = ?';
//        $sql = 'SELECT * FROM oauth_users WHERE username = :dni';
        $resultset = $this->adapter->query($sql, array($token, $dni));

        $sql = 'SELECT username, email, token_tmp,first_name from oauth_users where username = ?';
        $resultset = $this->adapter->query($sql, array($dni));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }
//        var_dump($token);
        return $data[0];
    }

    public function changePassword($token, $new_password) {

        $sql = 'update oauth_users '
                . 'set password = ?'
                . 'where token_tmp = ?';

        $resultset = $this->adapter->query($sql, array($new_password, $token));

        $sql = 'SELECT username, email, token_tmp,first_name from oauth_users where token_tmp = ?';
        $resultset = $this->adapter->query($sql, array($token));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }
        return $data[0];
    }

    public function validar_clave_anterior($username) {

        $sql = 'select password1 from oauth_users where username=? ';
        $resultset = $this->adapter->query($sql, array($username));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }                
        return $data[0];
    }

  public function change_password_x_username($username, $actual_password, $new_password) {
        
        $password2 = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, '35e907c7bd0e7619ced8e8a1651ccecb', strtoupper($actual_password), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
        try{
        $sql = 'update oauth_users '
                . 'set password = ? ,'
                . 'password1 = ? '
                . 'where username = ? ';

        $resultset = $this->adapter->query($sql, array($new_password,$password2, $username));
        }  catch (\Exception $e){
            
        }
        $sql = 'SELECT user_id, username, email,first_name, last_name, display_name '
                . 'FROM oauth_users '
                . 'WHERE username = ? ';
        $resultset = $this->adapter->query($sql, array($username));       

        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }
        if (count($data) === 0) {
            return false;
        }
        return $data[0];
    }

}
