<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Comite;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Comite\ComiteEntity;
use Zend\Crypt\Password\Bcrypt;

/**
 * Description of ComiteMapper
 *
 * @author kratos
 */
class ComiteMapper {

    protected $adapter;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function fetchOne($dni) {

        $sql = 'SELECT username, email,first_name,password1 FROM oauth_users WHERE username=?';
        $resultset = $this->adapter->query($sql, array($dni));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }

        return $data[0];
    }

    public function updateUsuario($dni, $objUsuario) {
        
        try {
            //actualizar
            $state = 0;
            $sql = 'update oauth_users '
                    . 'set username = ?,'
                    . 'first_name = ?,'
                    . 'last_name = ?,'
                    . 'email = ?,'
                    . 'display_name = ?,'
                    . 'state = ? '
                    . 'where username = ?';

            $params = array(
                $objUsuario->username,
                $objUsuario->first_name,
                $objUsuario->last_name,
                $objUsuario->email,
                $objUsuario->display_name,
                $state,
                $dni,
            );

            $update = $this->adapter->query($sql, $params);
            return array('username' => 1, 'mensaje' => 'Se guardó correctamente.');
        } catch (\Exception $e) {
            return array('username' => 5, 'mensaje' => $e->getMessage());
        }
    }

    public function createUsuario($objUsuario) {
        $cadena = "([^a-h][j-j][m-n][p-r][t-z][0-9])";
        $clave = substr(eregi_replace($cadena, "", md5(rand())) .
                eregi_replace($cadena, "", md5(rand())) .
                eregi_replace($cadena, "", md5(rand())), 0, 6);
        $password2 = $encripta = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, '35e907c7bd0e7619ced8e8a1651ccecb', $clave, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));

        $bcrypt = new Bcrypt();
        $password = $bcrypt->create($clave);
        $state = 0;
        $acl_id=2;
        try {

            $sql = 'insert into oauth_users( username,first_name,last_name,email,display_name,state,password,password1,acl_id) values(?,?,?,?,?,?,?,?,?) ';
            $params = array(
                $objUsuario->username,
                $objUsuario->first_name,
                $objUsuario->last_name,
                $objUsuario->email,
                $objUsuario->display_name,
                $state,
                $password,
                $password2,
                $acl_id,
            );
            $insert = $this->adapter->query($sql, $params);

            return array('username' => 1, 'mensaje' => 'Se guardó correctamente.');
        } catch (\Exception $e) {
            return array('username' => 5, 'mensaje' => $e->getMessage());
        }
    }

}
