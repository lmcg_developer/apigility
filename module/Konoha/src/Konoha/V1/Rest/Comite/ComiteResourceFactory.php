<?php
namespace Konoha\V1\Rest\Comite;

class ComiteResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Konoha\V1\Rest\Comite\ComiteMapper');
        $config = $services->get('config');
        return new ComiteResource($mapper, $config);
    }
}
