<?php
namespace Konoha\V1\Rest\Users_acl;

class Users_aclResourceFactory
{
    public function __invoke($services)
    {
        return new Users_aclResource($services);
    }
}
