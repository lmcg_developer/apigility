<?php

namespace Konoha\V1\Rest\Recuperar_clave;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class Recuperar_claveResource extends AbstractResourceListener {

    protected $services;
    protected $mapperBody;
    protected $mapperOauth;
    protected $config;

    public function __construct($services) {
        $this->services = $services;
        $this->mapperBody = $services->get('Konoha\V1\Rest\Models\BodyCorreoMapper');
        $this->mapperOauth = $services->get('Konoha\V1\Rest\Models\OauthUserMapper');
        $this->config = $services->get('config');
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data) {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id) {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data) {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id) {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array()) {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data) {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data) {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data) {

        $validator = new \Zend\Validator\EmailAddress();
        if (!$validator->isValid($data->email)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Email inválido',
                'correo' => $data->email,
            );
        }

        $validator_digits = new \Zend\I18n\Validator\IsInt();
        if (!$validator_digits->isValid($data->acl)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'ACL inválido',
                'correo' => $data->acl,
            );
        }

        $data_user = $this->mapperOauth->fetchOne_email($id, $data->email);
        if (count($data_user) === 0) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'No existe el usuario indicado',
                'correo' => $data->email,
            );
        }

        $email_body = $this->mapperBody->getBodyCorreo($data->acl);
        if (count($email_body) === 0) {
            return array(
                'username' => $id,
                'estado' => -110,
                'mensaje' => 'No existe la plantilla de correo',
                'correo' => $data->email,
            );
        }

        try {

            $nombre_completo = $data_user['first_name'] . ' ' . $data_user['last_name'];

            $password_decrypt = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, '35e907c7bd0e7619ced8e8a1651ccecb', base64_decode($data_user['password1']), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));

            $a_buscar = array('{{{NOMBRE}}}', '{{{USUARIO}}}', '{{{CONTRASENA}}}');
            $a_replace = array(mb_strtoupper($nombre_completo), $data_user['username'], $password_decrypt);
            $html_body = str_replace($a_buscar, $a_replace, $email_body['cuerpo']);

            $message = new Message();
            $message->setEncoding("UTF-8");

            $html = new MimePart($html_body);
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->setParts(array($html));
            $message->addTo($data_user['email'])
                    ->addFrom($email_body['email_from'], $email_body['name_from'])
                    ->setSubject($email_body['titulo'])
                    ->setBody($body);

            $config = $this->config;
            $config_server = $config['email_server']['server_died'];

            $transport = new SmtpTransport();
            $transport->setOptions(new SmtpOptions($config_server));
            $transport->send($message);
        } catch (\Exception $ex) {
//            return array($ex->getMessage());
            return array(
                'username' => $data_user['username'],
                'estado' => -100,
                'mensaje' => $ex->getMessage(),
                'correo' => $data_user['email'],
            );
        }

        return array(
            'username' => $data_user['username'],
            'estado' => 100,
            'mensaje' => 'Se ha enviado la contraseña al correo indicado',
            'correo' => $data_user['email'],
        );

//        return array($html_body);
//        return $data_user->email;
//        return $data_user['email'];
//        return $data_user;
////        return $data_email_body->titulo;
////        return array($email_body['titulo']);
//        return $email_body;
//        return $this->mapperBody->getBodyCorreo($data->acl);
//        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }

}
