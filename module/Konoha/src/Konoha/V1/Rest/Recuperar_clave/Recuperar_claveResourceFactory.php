<?php

namespace Konoha\V1\Rest\Recuperar_clave;

class Recuperar_claveResourceFactory {

    public function __invoke($services) {
//        $mapper = $services->get('Konoha\V1\Rest\Models\BodyCorreoMapper');
//        $config = $services->get('config');
        return new Recuperar_claveResource($services);
//        return new Recuperar_claveResource($mapper, $config);
    }

}
