<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Models;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Update;

/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class UsersAclMapper {

    protected $adapter;
    protected $table_name;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
        $this->table_name = 'oauth_users_acl';
    }

    public function fetch($id, $params = []) {

        $data = [];
//        return array($params);

//        $data = array('id' => $id, 'acl' => $params['acl']);
        /*

          $select->from(array('f' => 'foo'))  // base table
         * 
          ->join(array('b' => 'bar'),     // join table with alias
         * 
          'f.foo_id = b.foo_id');         // join expression
         */

        try {
            $sql = new Sql($this->adapter);
            $select = new Select();
            $select->columns(array(
                        'id',
                        'username',
                        'acl_id',
                        'activo',
                    ))
                    ->from(['oua' => $this->table_name])
                    ->join(['acl' => 'oauth_acl'], 'oua.acl_id = acl.acl_id',['ruta'])
                    ->join(['us' => 'oauth_users'], 'us.username = oua.username',['first_name', 'last_name'])
                    ->where(array('oua.username' => $id,
                        'oua.acl_id' => $params['acl'])
            );
            
//            $str_sql = $sql->getSqlStringForSqlObject($select);
            
//            echo $str_sql;
//            exit;
//            return array($str_sql);
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $row) {
                array_push($data, $row);
            }
            if ($results->count() === 1) {
                return $data[0];
            }
            return $data;
        } catch (\Exception $ex) {
            return array(
                'estado' => -100,
                'mensaje' => 'Error: ' . $ex->getMessage(),
                'correo' => null,
            );
        }
        return $data;
    }

    public function fetchAll($params) {
//        return array($id);

        $data = array();
        return $data;

//        try {
//            $sql = new Sql($this->adapter);
//            $select = new Select('body_email');
//            $select->columns(array(
//                        'id',
//                        'cuerpo',
//                        'acl',
//                        'nombre',
//                        'titulo',
//                        'email_from',
//                        'name_from'
//                    ))
//                    ->where(array('acl' => $acl));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $results = $statement->execute();
//
//            foreach ($results as $row) {
//                array_push($data, $row);
//            }
//            if ($results->count() === 1) {
//                return $data[0];
//            }
//            return $data;
//        } catch (\Exception $ex) {
//            return array(
//                'estado' => -100,
//                'mensaje' => 'Error: ' . $ex->getMessage(),
//                'correo' => null,
//            );
//        }
    }

    public function getBodyCorreo($acl = 0) {

        $data = array();

        try {
            $sql = new Sql($this->adapter);
            $select = new Select('body_email');
            $select->columns(array(
                        'id',
                        'cuerpo',
                        'acl',
                        'nombre',
                        'titulo',
                        'email_from',
                        'name_from'
                    ))
                    ->where(array('acl' => $acl));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $row) {
                array_push($data, $row);
            }
            if ($results->count() === 1) {
                return $data[0];
            }
            return $data;
        } catch (\Exception $ex) {
            return array(
                'estado' => -100,
                'mensaje' => 'Error: ' . $ex->getMessage(),
                'correo' => null,
            );
        }
        if (!$data) {
            return false;
        }
        return $data;
    }

}
