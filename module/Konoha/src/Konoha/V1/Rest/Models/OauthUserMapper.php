<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Models;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Update;

/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class OauthUserMapper {

    protected $adapter;
    protected $table_name;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
        $this->table_name = 'oauth_users';
    }

    public function fetchOne($username) {
        //  $sql = 'SELECT user_id, username, email,first_name, last_name, display_name FROM oauth_users WHERE username = ?';

        $sql = 'SELECT user_id, a.username, email,first_name, last_name, display_name,ruta FROM oauth_users a inner join oauth_acl b on '
                . 'a.acl_id=b.acl_id WHERE a.username = ?';

        $resultset = $this->adapter->query($sql, array($username));
        $data = $resultset->toArray();
        // $str_datos = file_get_contents("\public\perfil\".$data[0]['ruta']);
        // $data_menu = json_decode($str_datos, true);

        if (!$data) {
            return false;
        }

//        $entity = new PasswordEntity();
//        $entity->exchangeArray($data[0]);
        return $data[0];
    }

    public function fetchOne_email($username, $email) {
        $data = array();

        try {
            $sql = new Sql($this->adapter);
            $select = new Select($this->table_name);
            $select->columns(array(
                        'user_id',
                        'username',
                        'first_name',
                        'last_name',
                        'email',
                        'password1'
                    ))
                    ->where(array('username' => $username,
                        'email' => $email));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $row) {
                array_push($data, $row);
            }
            if ($results->count() === 1) {
                return $data[0];
            }
            return $data;
        } catch (\Exception $ex) {
            return array(
                'estado' => -100,
                'mensaje' => 'Error: ' . $ex->getMessage(),
                'correo' => null,
            );
        }
        if (!$data) {
            return false;
        }
        return $data;


        //  $sql = 'SELECT user_id, username, email,first_name, last_name, display_name FROM oauth_users WHERE username = ?';
//
//        $sql = 'SELECT user_id, a.username, email,first_name, last_name, display_name,ruta FROM oauth_users a inner join oauth_acl b on '
//                . 'a.acl_id=b.acl_id WHERE a.username = ? and a.email = ?';
//
//        $resultset = $this->adapter->query($sql, array($username, $email));
//        $data = $resultset->toArray();
//        // $str_datos = file_get_contents("\public\perfil\".$data[0]['ruta']);
//        // $data_menu = json_decode($str_datos, true);
//
//        if (!$data) {
//            return false;
//        }
//
////        $entity = new PasswordEntity();
////        $entity->exchangeArray($data[0]);
//        return $data[0];
    }

    public function changePassword($username, $new_password) {

        $sql = 'update oauth_users '
                . 'set password = ?'
                . 'where username = ?';

        $resultset = $this->adapter->query($sql, array($new_password, $username));

        $sql = 'SELECT user_id, username, email,first_name, last_name, display_name FROM oauth_users WHERE username = ?';
        $resultset = $this->adapter->query($sql, array($username));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }
        return $data[0];
    }

    public function cambiar_correo($username = '', $email = '') {

        $data = array();

        try {
            $sql = new Sql($this->adapter);
            $update = new Update($this->table_name);
            $update->set(array('email' => $email))
                    ->where(array('username' => $username))
            ;
//            $update_sql = $sql->getSqlStringForSqlObject($update);
//            $results = $this->adapter->query($update_sql);
//            echo $update_sql;
//            var_dump($results);
            $statement = $sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
//            $sql = 'update oauth_users '
//                    . 'set password = ?'
//                    . 'where username = ?';
//            $resultset = $this->adapter->query($sql, array($new_password, $username));
//
//            $sql = 'SELECT user_id, username, email,first_name, last_name, display_name FROM oauth_users WHERE username = ?';
//            $resultset = $this->adapter->query($sql, array($username));
//            $data = $results->toArray();
//            return $results;

            $select = new Select($this->table_name);
            $select->columns(array('username', 'email'))
                    ->where(array('username' => $username));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() === 0) {
                return array(
                    'estado' => -100,
                    'mensaje' => 'No existe',
                    'correo' => null,
                );
            }

            if ($results->count() === 1) {
                return array(
                    'estado' => 100,
                    'mensaje' => 'Se actualizó correctamente',
                    'correo' => $email
                );
            }
        } catch (\Exception $ex) {
            return array(
                'estado' => -100,
                'mensaje' => 'Error: ' . $ex->getMessage(),
                'correo' => null,
            );
        }
        if (!$data) {
            return false;
        }
        return $data;
    }

}
