<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Konoha\V1\Rest\Models\UserMapper

namespace Konoha\V1\Rest\Models;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Insert;

/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class UserMapper {

    protected $table = 'oauth_users';
    protected $adapter;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function fetchOne($dni) {

        $sql = 'SELECT username, email, first_name FROM oauth_users WHERE username = ?';
//        $sql = 'SELECT * FROM oauth_users WHERE username = :dni';
        $resultset = $this->adapter->query($sql, array($dni));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }

        return $data;
        /////////////
//        $adapter = $this->adapter;
//        $sql = new Sql($this->$adapter);
////        $data = array();
//
//        $select = $sql->select();
//        $select->from($this->table);
//        $selectString = $sql->getSqlStringForSqlObject($select);
////        echo($selectString);
//        $resultSet = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
//        $data = $resultset->toArray();
//        return $data;
//        $sql = 'SELECT username, email FROM oauth_users WHERE username = ?';
////        $sql = 'SELECT * FROM oauth_users WHERE username = :dni';
//        $resultset = $this->adapter->query($sql, array($dni));
//        $data = $resultset->toArray();
//        if (!$data) {
//            return false;
//        }
//
////        $entity = new PasswordEntity();
////        $entity->exchangeArray($data[0]);
//        return $data;
    }

}
