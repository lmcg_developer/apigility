<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Models;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;

/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class UsuarioMapper {

    protected $adapter;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function fetchOne($username) {
      //  $sql = 'SELECT user_id, username, email,first_name, last_name, display_name FROM oauth_users WHERE username = ?';

        $sql = 'SELECT user_id, a.username, email,first_name, last_name, display_name,ruta FROM oauth_users a inner join oauth_acl b on '
                . 'a.acl_id=b.acl_id WHERE a.username = ?';

        $resultset = $this->adapter->query($sql, array($username));
        $data = $resultset->toArray();
        // $str_datos = file_get_contents("\public\perfil\".$data[0]['ruta']);
        // $data_menu = json_decode($str_datos, true);

        if (!$data) {
            return false;
        }

//        $entity = new PasswordEntity();
//        $entity->exchangeArray($data[0]);
        return $data[0];
    }

    public function changePassword($username, $new_password) {

        $sql = 'update oauth_users '
                . 'set password = ?'
                . 'where username = ?';

        $resultset = $this->adapter->query($sql, array($new_password, $username));

        $sql = 'SELECT user_id, username, email,first_name, last_name, display_name FROM oauth_users WHERE username = ?';
        $resultset = $this->adapter->query($sql, array($username));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }
        return $data[0];
    }

}
