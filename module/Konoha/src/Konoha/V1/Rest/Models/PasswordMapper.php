<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Password;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;
/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class PasswordMapper {

    protected $adapter;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function fetchOne($dni) {
        $sql = 'SELECT username, email FROM oauth_users WHERE username = ?';
//        $sql = 'SELECT * FROM oauth_users WHERE username = :dni';
        $resultset = $this->adapter->query($sql, array($dni));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }

//        $entity = new PasswordEntity();
//        $entity->exchangeArray($data[0]);
        return $data;
    }

}
