<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Models;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Password\PasswordEntity;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Update;

/**
 * Description of PasswordMapper
 *
 * @author kratos
 */
class BodyCorreoMapper {

    protected $adapter;
    protected $table_name;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
        $this->table_name = 'body_email';
    }

    public function getBodyCorreo($acl = 0) {

        $data = array();

        try {
            $sql = new Sql($this->adapter);
            $select = new Select('body_email');
            $select->columns(array(
                        'id',
                        'cuerpo',
                        'acl',
                        'nombre',
                        'titulo',
                        'email_from',
                        'name_from'
                    ))
                    ->where(array('acl' => $acl));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $row) {
                array_push($data, $row);
            }
            if ($results->count() === 1) {
//                var_dump($data[0]);
                return $data[0];
            }
            return $data;


//            if ($results->count() === 0) {
//                return array(
//                    'estado' => -100,
//                    'mensaje' => 'No existe',
//                    'correo' => null,
//                );
//            }
//
//            if ($results->count() === 1) {
//                return array(
//                    'estado' => 100,
//                    'mensaje' => 'Se actualizó correctamente',
//                    'correo' => $results
//                );
//            }
        } catch (\Exception $ex) {
            return array(
                'estado' => -100,
                'mensaje' => 'Error: ' . $ex->getMessage(),
                'correo' => null,
            );
        }
        if (!$data) {
            return false;
        }
        return $data;
    }

}
