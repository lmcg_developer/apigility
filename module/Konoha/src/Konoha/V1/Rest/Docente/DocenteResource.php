<?php

namespace Konoha\V1\Rest\Docente;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Crypt\Password\Bcrypt;

class DocenteResource extends AbstractResourceListener {

    protected $mapper;
    protected $config;

    public function __construct($mapper, $config) {
        $this->mapper = $mapper;
        $this->config = $config;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data) {

        $response = new \stdClass();

        $password = $this->mapper->createDocente($data);
        $response->codigo = 0;

        if ($password === FALSE) {
            return array('codigo' => -100, 'mensaje' => 'No se encontró el usuario');
        }
        if ($password['email'] === '') {
            return array('codigo' => -100, 'mensaje' => 'No tiene correo registrado');
        }
        $clave = $desencripta = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, '35e907c7bd0e7619ced8e8a1651ccecb', base64_decode($password['password_1']), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));

        $config_email_server = $this->config['email_accounts3']['cambiar_clave']['config_server'];
        $config_email_message = $this->config['email_accounts3']['cambiar_clave']['config_message'];
        $message = new Message();

        $htmlMarkup = 'Estimado <b>' . $password['first_name'] . ': </b> '
                . '<p>Tu usuario de acceso al aplicativo es:' . $password['username'] . '</p><br>'
                . '<p>Tu clave de acceso al aplicativo es:' . $clave . '</p>';


        $html = new MimePart($htmlMarkup);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($html));

        $message->addTo($password['email'])
                ->addFrom($config_email_message['from'])
                ->setSubject($config_email_message['subject'])
                ->setBody($body);

        $transport = new SmtpTransport();

        $options = new SmtpOptions($config_email_server);
        try {
            $transport->setOptions($options);
            $transport->send($message);
            return array('codigo' => 'correcto', 'mensaje' => 'Se creó la cuenta correctamente.');
        } catch (\Zend\Mail\Transport\Exception $e) {
            return array('codigo' => 'error', 'mensaje' => 'Ocurrió algún error al enviar la contraseña.');
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id) {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data) {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id) {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array()) {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data) {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data) {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data) {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }

}
