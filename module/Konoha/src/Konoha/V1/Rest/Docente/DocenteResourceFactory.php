<?php

namespace Konoha\V1\Rest\Docente;

class DocenteResourceFactory {

    public function __invoke($services) {
        $mapper = $services->get('Konoha\V1\Rest\Docente\DocenteMapper');
        $config = $services->get('config');
        return new DocenteResource($mapper, $config);
    }

}
