<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Konoha\V1\Rest\Docente;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Konoha\V1\Rest\Docente\DocenteEntity;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\Adapter\Driver\DriverInterface;

/**
 * Description of DocenteMapper
 *
 * @author kratos
 */
class DocenteMapper {

    protected $adapter;

    public function __construct(AdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function generateTokenDocente($id, $dni) {

        $token = md5(time() . 'MINEDU');
        $sql = 'update oauth_users_temp '
                . 'set token_tmp = ?'
                . 'where username = ? and user_id = ?';

        $resultset = $this->adapter->query($sql, array($token, $dni, $id));

        $sql = 'SELECT username, email, token_tmp,first_name from oauth_users_temp where username = ? and user_id = ?';
        $resultset = $this->adapter->query($sql, array($dni, $id));
        $data = $resultset->toArray();
        if (!$data) {
            return false;
        }

        return $data[0];
    }

    public function createDocente($objUsuario) {
        //var_dump($objUsuario);

        $cadena = "([^a-h][j-j][m-n][p-r][t-z][0-9])";
        $clave = substr(eregi_replace($cadena, "", md5(rand())) .
                eregi_replace($cadena, "", md5(rand())) .
                eregi_replace($cadena, "", md5(rand())), 0, 6);
        $password2 = $encripta = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, '35e907c7bd0e7619ced8e8a1651ccecb', $clave, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));

        $bcrypt = new Bcrypt();
        $password = $bcrypt->create($clave);
        $state = 0;
        $acl_acl_id = 4; //(DOCENTE)

        try {

            $sql = 'insert into oauth_users_temp( username,password,password_1,first_name,last_name,apellido_materno,email,fecha_nacimiento,display_name,state,acl_acl_id) values(?,?,?,?,?,?,?,?,?,?,?) ';
            $params = array(
                $objUsuario->username,
                $password,
                $password2,
                $objUsuario->first_name,
                $objUsuario->last_name,
                $objUsuario->apellido_materno,
                $objUsuario->email,
                $objUsuario->fecha_nacimiento,
                $objUsuario->display_name,
                $state,
                $acl_acl_id,
            );
            $insert = $this->adapter->query($sql, $params);
            $lastId = $this->adapter->getDriver()->getLastGeneratedValue();

            if($lastId!=='' || $lastId!==FALSE || $lastId!==0) {
                $sql = 'SELECT username, email,first_name,password_1 FROM oauth_users_temp WHERE user_id=?';
                $resultset = $this->adapter->query($sql, array($lastId));
                $data = $resultset->toArray();
                if (!$data) {
                    return false;
                }
                return $data[0];
            }

           // return array('id' => 100, 'mensaje' => 'Se guardó correctamente.');
        } catch (\Exception $e) {
            return array('id' => -101, 'mensaje' => $e->getMessage());
        }
    }

}
