<?php
namespace Konoha\V1\Rest\Cambiarcorreo;

class CambiarcorreoResourceFactory
{
    public function __invoke($services)
    {
         $mapper = $services->get('Konoha\V1\Rest\Models\OauthUserMapper');
        $config = $services->get('config');
        return new CambiarcorreoResource($mapper, $config);
    }
}
