<?php

namespace Konoha\V1\Rest\Cambiarcorreo;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class CambiarcorreoResource extends AbstractResourceListener {

    protected $mapper;
    protected $config;

    public function __construct($mapper, $config) {
        $this->mapper = $mapper;
        $this->config = $config;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data) {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id) {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data) {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id) {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array()) {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data) {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data) {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data) {
        
//        return array();

        $validemptyid = new \Zend\Validator\NotEmpty();
        if (!$validemptyid->isValid($id)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Error: El número de documento ingresado es vacío.',
                'correo' => null,
            );
        }
        
        if (!$validemptyid->isValid($data->email)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Error: El correo ingresado es vacío.',
                'correo' => null,
            );
        }

        $validator_digits = new \Zend\I18n\Validator\IsInt();
        if (!$validator_digits->isValid($id)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Error: El número de documento no es válido.',
                'correo' => null,
            );
        }

        $validatormin = new \Zend\Validator\StringLength(array('min' => 8));
        if (!$validatormin->isValid($id)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Error: El número de documento tiene menos de 8 dígitos.',
                'correo' => null,
            );
        }
        $validatormax = new \Zend\Validator\StringLength(array('max' => 9));
        if (!$validatormax->isValid($id)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Error: El número de documento tiene mas de 9 dígitos.',
                'correo' => null,
            );
        }

        $validatormail = new \Zend\Validator\EmailAddress();
        if (!$validatormail->isValid($data->email)) {
            return array(
                'username' => $id,
                'estado' => -100,
                'mensaje' => 'Error: El correo no es válido',
                'correo' => null,
            );
        }

        $result = $this->mapper->cambiar_correo($id, $data->email);
        return $result;
    }

}
