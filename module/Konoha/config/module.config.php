<?php
return array(
    'controllers' => array(
        'factories' => array(
            'Konoha\\V1\\Rpc\\Hokage\\Controller' => 'Konoha\\V1\\Rpc\\Hokage\\HokageControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'konoha.rpc.hokage' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/konoha/hokage',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rpc\\Hokage\\Controller',
                        'action' => 'hokage',
                    ),
                ),
            ),
            'konoha.rest.oauth-users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/oauth_users[/:oauth_users_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\OauthUsers\\Controller',
                    ),
                ),
            ),
            'konoha.rest.password' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/password[/:password_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Password\\Controller',
                    ),
                ),
            ),
            'konoha.rest.usuario' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/usuario[/:usuario_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Usuario\\Controller',
                    ),
                ),
            ),
            'konoha.rest.comite' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/comite[/:comite_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Comite\\Controller',
                    ),
                ),
            ),
            'konoha.rest.docente' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/docente[/:docente_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Docente\\Controller',
                    ),
                ),
            ),
            'konoha.rest.cambiarpass' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/cambiarpass[/:cambiarpass_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Cambiarpass\\Controller',
                    ),
                ),
            ),
            'konoha.rest.recuperar_clave' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/recuperar_clave[/:recuperar_clave_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Recuperar_clave\\Controller',
                    ),
                ),
            ),
            'konoha.rest.cambiarcorreo' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/cambiarcorreo[/:cambiarcorreo_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Cambiarcorreo\\Controller',
                    ),
                ),
            ),
            'konoha.rest.users_acl' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users_acl[/:users_acl_id]',
                    'defaults' => array(
                        'controller' => 'Konoha\\V1\\Rest\\Users_acl\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'konoha.rpc.hokage',
            1 => 'konoha.rest.oauth-users',
            2 => 'konoha.rest.password',
            3 => 'konoha.rest.usuario',
            4 => 'konoha.rest.comite',
            5 => 'konoha.rest.docente',
            6 => 'konoha.rest.cambiarpass',
            7 => 'konoha.rest.recuperar_clave',
            8 => 'konoha.rest.cambiarcorreo',
            9 => 'konoha.rest.users_acl',
        ),
    ),
    'zf-rpc' => array(
        'Konoha\\V1\\Rpc\\Hokage\\Controller' => array(
            'service_name' => 'hokage',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'konoha.rpc.hokage',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Konoha\\V1\\Rpc\\Hokage\\Controller' => 'Json',
            'Konoha\\V1\\Rest\\OauthUsers\\Controller' => 'HalJson',
            'Konoha\\V1\\Rest\\Password\\Controller' => 'HalJson',
            'Konoha\\V1\\Rest\\Usuario\\Controller' => 'HalJson',
            'Konoha\\V1\\Rest\\Comite\\Controller' => 'HalJson',
            'Konoha\\V1\\Rest\\Docente\\Controller' => 'HalJson',
            'Konoha\\V1\\Rest\\Cambiarpass\\Controller' => 'Json',
            'Konoha\\V1\\Rest\\Recuperar_clave\\Controller' => 'HalJson',
            'Konoha\\V1\\Rest\\Cambiarcorreo\\Controller' => 'Json',
            'Konoha\\V1\\Rest\\Users_acl\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Konoha\\V1\\Rpc\\Hokage\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'Konoha\\V1\\Rest\\OauthUsers\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Password\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Usuario\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Comite\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Docente\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Cambiarpass\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Recuperar_clave\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Cambiarcorreo\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Users_acl\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Konoha\\V1\\Rpc\\Hokage\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\OauthUsers\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Password\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Usuario\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Comite\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Docente\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Cambiarpass\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Recuperar_clave\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Cambiarcorreo\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
            'Konoha\\V1\\Rest\\Users_acl\\Controller' => array(
                0 => 'application/vnd.konoha.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-rest' => array(
        'Konoha\\V1\\Rest\\OauthUsers\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\OauthUsers\\OauthUsersResource',
            'route_name' => 'konoha.rest.oauth-users',
            'route_identifier_name' => 'oauth_users_id',
            'collection_name' => 'oauth_users',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\OauthUsers\\OauthUsersEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\OauthUsers\\OauthUsersCollection',
            'service_name' => 'oauth_users',
        ),
        'Konoha\\V1\\Rest\\Password\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Password\\PasswordResource',
            'route_name' => 'konoha.rest.password',
            'route_identifier_name' => 'password_id',
            'collection_name' => 'password',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Password\\PasswordEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Password\\PasswordCollection',
            'service_name' => 'password',
        ),
        'Konoha\\V1\\Rest\\Usuario\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Usuario\\UsuarioResource',
            'route_name' => 'konoha.rest.usuario',
            'route_identifier_name' => 'usuario_id',
            'collection_name' => 'usuario',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Usuario\\UsuarioEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Usuario\\UsuarioCollection',
            'service_name' => 'usuario',
        ),
        'Konoha\\V1\\Rest\\Comite\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Comite\\ComiteResource',
            'route_name' => 'konoha.rest.comite',
            'route_identifier_name' => 'comite_id',
            'collection_name' => 'comite',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'PATCH',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Comite\\ComiteEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Comite\\ComiteCollection',
            'service_name' => 'comite',
        ),
        'Konoha\\V1\\Rest\\Docente\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Docente\\DocenteResource',
            'route_name' => 'konoha.rest.docente',
            'route_identifier_name' => 'docente_id',
            'collection_name' => 'docente',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
                3 => 'PATCH',
                4 => 'DELETE',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Docente\\DocenteEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Docente\\DocenteCollection',
            'service_name' => 'docente',
        ),
        'Konoha\\V1\\Rest\\Cambiarpass\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassResource',
            'route_name' => 'konoha.rest.cambiarpass',
            'route_identifier_name' => 'cambiarpass_id',
            'collection_name' => 'cambiarpass',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
                3 => 'PATCH',
                4 => 'DELETE',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassCollection',
            'service_name' => 'cambiarpass',
        ),
        'Konoha\\V1\\Rest\\Recuperar_clave\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveResource',
            'route_name' => 'konoha.rest.recuperar_clave',
            'route_identifier_name' => 'recuperar_clave_id',
            'collection_name' => 'recuperar_clave',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveCollection',
            'service_name' => 'recuperar_clave',
        ),
        'Konoha\\V1\\Rest\\Cambiarcorreo\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoResource',
            'route_name' => 'konoha.rest.cambiarcorreo',
            'route_identifier_name' => 'cambiarcorreo_id',
            'collection_name' => 'cambiarcorreo',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
                3 => 'PATCH',
                4 => 'DELETE',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoCollection',
            'service_name' => 'cambiarcorreo',
        ),
        'Konoha\\V1\\Rest\\Users_acl\\Controller' => array(
            'listener' => 'Konoha\\V1\\Rest\\Users_acl\\Users_aclResource',
            'route_name' => 'konoha.rest.users_acl',
            'route_identifier_name' => 'users_acl_id',
            'collection_name' => 'users_acl',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Konoha\\V1\\Rest\\Users_acl\\Users_aclEntity',
            'collection_class' => 'Konoha\\V1\\Rest\\Users_acl\\Users_aclCollection',
            'service_name' => 'users_acl',
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Konoha\\V1\\Rest\\OauthUsers\\OauthUsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.oauth-users',
                'route_identifier_name' => 'oauth_users_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\OauthUsers\\OauthUsersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.oauth-users',
                'route_identifier_name' => 'oauth_users_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Password\\PasswordEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.password',
                'route_identifier_name' => 'password_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ObjectProperty',
            ),
            'Konoha\\V1\\Rest\\Password\\PasswordCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.password',
                'route_identifier_name' => 'password_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Usuario\\UsuarioEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.usuario',
                'route_identifier_name' => 'usuario_id',
                'hydrator' => 'ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Usuario\\UsuarioCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.usuario',
                'route_identifier_name' => 'usuario_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Comite\\ComiteEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.comite',
                'route_identifier_name' => 'comite_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Comite\\ComiteCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.comite',
                'route_identifier_name' => 'comite_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Docente\\DocenteEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.docente',
                'route_identifier_name' => 'docente_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Docente\\DocenteCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.docente',
                'route_identifier_name' => 'docente_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.cambiarpass',
                'route_identifier_name' => 'cambiarpass_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.cambiarpass',
                'route_identifier_name' => 'cambiarpass_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.recuperar_clave',
                'route_identifier_name' => 'recuperar_clave_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.recuperar_clave',
                'route_identifier_name' => 'recuperar_clave_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.cambiarcorreo',
                'route_identifier_name' => 'cambiarcorreo_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.cambiarcorreo',
                'route_identifier_name' => 'cambiarcorreo_id',
                'is_collection' => true,
            ),
            'Konoha\\V1\\Rest\\Users_acl\\Users_aclEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.users_acl',
                'route_identifier_name' => 'users_acl_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Konoha\\V1\\Rest\\Users_acl\\Users_aclCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'konoha.rest.users_acl',
                'route_identifier_name' => 'users_acl_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'db-connected' => array(
            'Konoha\\V1\\Rest\\OauthUsers\\OauthUsersResource' => array(
                'adapter_name' => 'oauthMySql',
                'table_name' => 'oauth_users',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'Konoha\\V1\\Rest\\OauthUsers\\Controller',
                'entity_identifier_name' => 'id',
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Konoha\\V1\\Rest\\Password\\PasswordResource' => 'Konoha\\V1\\Rest\\Password\\PasswordResourceFactory',
            'Konoha\\V1\\Rest\\Usuario\\UsuarioResource' => 'Konoha\\V1\\Rest\\Usuario\\UsuarioResourceFactory',
            'Konoha\\V1\\Rest\\Comite\\ComiteResource' => 'Konoha\\V1\\Rest\\Comite\\ComiteResourceFactory',
            'Konoha\\V1\\Rest\\Docente\\DocenteResource' => 'Konoha\\V1\\Rest\\Docente\\DocenteResourceFactory',
            'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassResource' => 'Konoha\\V1\\Rest\\Cambiarpass\\CambiarpassResourceFactory',
            'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveResource' => 'Konoha\\V1\\Rest\\Recuperar_clave\\Recuperar_claveResourceFactory',
            'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoResource' => 'Konoha\\V1\\Rest\\Cambiarcorreo\\CambiarcorreoResourceFactory',
            'Konoha\\V1\\Rest\\Users_acl\\Users_aclResource' => 'Konoha\\V1\\Rest\\Users_acl\\Users_aclResourceFactory',
        ),
    ),
    'zf-content-validation' => array(
        'Konoha\\V1\\Rest\\Password\\Controller' => array(
            'input_filter' => 'Konoha\\V1\\Rest\\Password\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Konoha\\V1\\Rest\\Password\\Validator' => array(
            0 => array(
                'name' => 'url',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
                'allow_empty' => false,
                'continue_if_empty' => false,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'Konoha\\V1\\Rest\\OauthUsers\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Konoha\\V1\\Rest\\Password\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Konoha\\V1\\Rpc\\Hokage\\Controller' => array(
                'actions' => array(
                    'hokage' => array(
                        'GET' => false,
                        'POST' => false,
                        'PATCH' => false,
                        'PUT' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
        ),
    ),
);
