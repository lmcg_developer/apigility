<?php
namespace Usuarios\V1\Rest\Listar;

class ListarResourceFactory
{
    public function __invoke($services)
    {
        return new ListarResource();
    }
}
