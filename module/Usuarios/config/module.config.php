<?php
return array(
    'router' => array(
        'routes' => array(
            'usuarios.rest.listar' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/listar[/:listar_id]',
                    'defaults' => array(
                        'controller' => 'Usuarios\\V1\\Rest\\Listar\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'usuarios.rest.listar',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Usuarios\\V1\\Rest\\Listar\\ListarResource' => 'Usuarios\\V1\\Rest\\Listar\\ListarResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'Usuarios\\V1\\Rest\\Listar\\Controller' => array(
            'listener' => 'Usuarios\\V1\\Rest\\Listar\\ListarResource',
            'route_name' => 'usuarios.rest.listar',
            'route_identifier_name' => 'listar_id',
            'collection_name' => 'listar',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Usuarios\\V1\\Rest\\Listar\\ListarEntity',
            'collection_class' => 'Usuarios\\V1\\Rest\\Listar\\ListarCollection',
            'service_name' => 'listar',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Usuarios\\V1\\Rest\\Listar\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Usuarios\\V1\\Rest\\Listar\\Controller' => array(
                0 => 'application/vnd.usuarios.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Usuarios\\V1\\Rest\\Listar\\Controller' => array(
                0 => 'application/vnd.usuarios.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Usuarios\\V1\\Rest\\Listar\\ListarEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'usuarios.rest.listar',
                'route_identifier_name' => 'listar_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Usuarios\\V1\\Rest\\Listar\\ListarCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'usuarios.rest.listar',
                'route_identifier_name' => 'listar_id',
                'is_collection' => true,
            ),
        ),
    ),
);
