<?php
return array(
    'controllers' => array(
        'factories' => array(
            'ping\\V1\\Rpc\\Ping\\Controller' => 'ping\\V1\\Rpc\\Ping\\PingControllerFactory',
            'ping\\V2\\Rpc\\Ping\\Controller' => 'ping\\V2\\Rpc\\Ping\\PingControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'ping.rpc.ping' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/ping',
                    'defaults' => array(
                        'controller' => 'ping\\V1\\Rpc\\Ping\\Controller',
                        'action' => 'ping',
                    ),
                ),
            ),
            'ping.rest.usuarios' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/usuarios[/:usuarios_id]',
                    'defaults' => array(
                        'controller' => 'ping\\V2\\Rest\\Usuarios\\Controller',
                    ),
                ),
            ),
            'ping.rest.users_acl' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users_acl[/:users_acl_id]',
                    'defaults' => array(
                        'controller' => 'ping\\V2\\Rest\\Users_acl\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'ping.rpc.ping',
            1 => 'ping.rest.usuarios',
            2 => 'ping.rest.users_acl',
        ),
        'default_version' => 1,
    ),
    'zf-rpc' => array(
        'ping\\V1\\Rpc\\Ping\\Controller' => array(
            'service_name' => 'ping',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'ping.rpc.ping',
        ),
        'ping\\V2\\Rpc\\Ping\\Controller' => array(
            'service_name' => 'ping',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'ping.rpc.ping',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'ping\\V1\\Rpc\\Ping\\Controller' => 'Json',
            'ping\\V2\\Rpc\\Ping\\Controller' => 'Json',
            'ping\\V2\\Rest\\Usuarios\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'ping\\V1\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.ping.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'ping\\V2\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.ping.v2+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'ping\\V2\\Rest\\Usuarios\\Controller' => array(
                0 => 'application/vnd.ping.v2+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'ping\\V1\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.ping.v1+json',
                1 => 'application/json',
            ),
            'ping\\V2\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.ping.v2+json',
                1 => 'application/json',
            ),
            'ping\\V2\\Rest\\Usuarios\\Controller' => array(
                0 => 'application/vnd.ping.v2+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'ping\\V1\\Rpc\\Ping\\Controller' => array(
            'input_filter' => 'ping\\V1\\Rpc\\Ping\\Validator',
        ),
        'ping\\V2\\Rpc\\Ping\\Controller' => array(
            'input_filter' => 'ping\\V2\\Rpc\\Ping\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'ping\\V1\\Rpc\\Ping\\Validator' => array(
            0 => array(
                'name' => 'ack',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
                'continue_if_empty' => true,
            ),
        ),
        'ping\\V2\\Rpc\\Ping\\Validator' => array(
            0 => array(
                'name' => 'ack',
                'required' => '1',
                'filters' => array(),
                'validators' => array(),
                'continue_if_empty' => '1',
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'ping\\V2\\Rest\\Usuarios\\UsuariosResource' => 'ping\\V2\\Rest\\Usuarios\\UsuariosResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'ping\\V2\\Rest\\Usuarios\\Controller' => array(
            'listener' => 'ping\\V2\\Rest\\Usuarios\\UsuariosResource',
            'route_name' => 'ping.rest.usuarios',
            'route_identifier_name' => 'usuarios_id',
            'collection_name' => 'usuarios',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'ping\\V2\\Rest\\Usuarios\\UsuariosEntity',
            'collection_class' => 'ping\\V2\\Rest\\Usuarios\\UsuariosCollection',
            'service_name' => 'usuarios',
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'ping\\V2\\Rest\\Usuarios\\UsuariosEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'ping.rest.usuarios',
                'route_identifier_name' => 'usuarios_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'ping\\V2\\Rest\\Usuarios\\UsuariosCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'ping.rest.usuarios',
                'route_identifier_name' => 'usuarios_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'ping\\V2\\Rest\\Usuarios\\Controller' => array(
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'ping\\V2\\Rpc\\Ping\\Controller' => array(
                'actions' => array(
                    'ping' => array(
                        'GET' => false,
                        'POST' => false,
                        'PATCH' => false,
                        'PUT' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
        ),
    ),
);
