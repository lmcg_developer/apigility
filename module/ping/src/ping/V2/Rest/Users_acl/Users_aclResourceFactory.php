<?php
namespace ping\V2\Rest\Users_acl;

class Users_aclResourceFactory
{
    public function __invoke($services)
    {
        return new Users_aclResource();
    }
}
