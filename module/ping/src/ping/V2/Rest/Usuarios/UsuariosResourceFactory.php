<?php
namespace ping\V2\Rest\Usuarios;

class UsuariosResourceFactory
{
    public function __invoke($services)
    {
        return new UsuariosResource();
    }
}
